﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class AccountLogic : CoreLogic<Account>
    {
        IService _service;
        public AccountLogic(IService service):base(service)
        {
            _service = service;
        }
    }
}
