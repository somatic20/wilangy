﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class GenderLogic:CoreLogic<Gender>
    {
        IService service;
        public GenderLogic(IService _service) : base(_service)
        {
            service = _service;
        }
    }
}
