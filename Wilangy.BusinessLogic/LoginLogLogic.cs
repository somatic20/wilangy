﻿using Microsoft.AspNetCore.Identity;
using Object.Jwt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class LoginLogLogic : CoreLogic<LoginLog>
    {
        IJwtFactory jwtFactory;
        UserManager<Person> userManager;
        JwtIssuerOptions jwtIssuerOptions;
        IService service;
        public LoginLogLogic(UserManager<Person> _userManager, IJwtFactory _jwtFactory, JwtIssuerOptions _jwtIssuerOptions, IService _service) : base(_service)
        {
            userManager = _userManager;
            jwtFactory = _jwtFactory;
            jwtIssuerOptions = _jwtIssuerOptions;
            service = _service;
        }
        public async Task<LoginData> LoginStaff(LoginDetails loginDTO)
        {
            Person user = await userManager.FindByEmailAsync(loginDTO.EmailAddress);
            if (user != null)
            {
                bool verifiedUser = await userManager.CheckPasswordAsync(user, loginDTO.Password);
                if (verifiedUser)
                {
                    var identity = jwtFactory.GenerateClaimsIdentity(user.UserName, user.Id);
                    if (identity == null)
                    {
                        return null;
                    }
                    jwtObject jwt = new jwtObject();
                    var role = await userManager.GetRolesAsync(user);
                    string staffType = Enum.GetValues(typeof(EnumClass.StaffType)).Cast<EnumClass.StaffType>().Where(p => (int)p == user.PersonTypeId).Select(v => v.ToString()).FirstOrDefault();
                    jwt = await Tokens.GenerateJwt(identity, jwtFactory, user, staffType, jwtIssuerOptions);
                    LoginLog loginLog = AddEntity(new LoginLog() { LoginTime = DateTime.UtcNow, UserID = user.Id });
                    LoginData loginData = new LoginData();
                    loginData.AuthToken = jwt.auth_token;
                    loginData.Id = jwt.id;
                    loginData.ExpirationTime = jwt.expires_in;
                    loginData.Fullname = user.Fullname;
                    loginData.PersonType = user.PersonTypeId;
                    loginData.Email = user.Email;
                    loginData.UserId = user.Id;
                    loginData.PhoneNumber = user.PhoneNumber;
                    loginData.Role = role[0];
                    int saveCount = service.Save();

                    return loginData;
                }
            }
            return null;
        }
    }
}
