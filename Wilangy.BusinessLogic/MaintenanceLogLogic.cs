﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class MaintenanceLogLogic:CoreLogic<MaintenanceLog>
    {
        IService _service;
        public MaintenanceLogLogic(IService service) : base(service)
        {
            _service = service;
        }
    }
}
