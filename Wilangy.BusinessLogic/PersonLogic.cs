﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class PersonLogic : CoreLogic<Person>
    {
        IService service;
        UserManager<Person> _userManager;

        public PersonLogic(IService _service, UserManager<Person> userManager) : base(_service)
        {
            service = _service;
            _userManager = userManager;
        }


        public Person CreateIdentityUser(PersonDTO model)
        {
            Person personCheck = GetEntityBy(p => p.Email == model.Email);

            if (personCheck == null)
            {
                Person person = new Person
                {
                    ContactAddress = model.ContactAddress,
                    DateOfBirth = model.DateOfBirth == null ? new DateTime(): model.DateOfBirth,
                    Email = model.Email,
                    Firstname = model.Firstname,
                    GenderId = model.GenderId,
                    Lastname = model.Lastname,
                    Lga = model.Lga,
                    Othername = model.Othername == "undefined" || model.Othername == "null" ? "": model.Othername,
                    PersonTypeId = model.PersonTypeId,
                    PhoneNumber = model.PhoneNumber,
                    PictureUrl = model.PictureUrl,
                    EnteredBy = model.EnteredBy,
                    UserName = model.Email,
                    Religion = model.Religion,
                    OtherReligion = model.OtherReligion,
                    StateOfOrigin = model.StateOfOrigin
                };

                return person;
            }
            return null;
        }

        public UserViewModel CompleteUserDetails(Person person, Staff staff)
        {
            return new UserViewModel
            {
                Person = person,
                Staff = staff
            };
        }

        public Person UpdatePerson(PersonDTO model)
        {
            Person person = GetEntityBy(p => p.Email == model.Email);
            if (person != null)
            {
                person.Firstname = model.Firstname;
                person.GenderId = model.GenderId;
                person.Lastname = model.Lastname;
                person.Lga = model.Lga;
                person.Othername = model.Othername;
                person.PersonTypeId = model.PersonTypeId;
                person.PhoneNumber = model.PhoneNumber;
                person.PictureUrl = model.PictureUrl;
                person.StateOfOrigin = model.StateOfOrigin;
                person.DateOfBirth = model.DateOfBirth;
                person.OtherReligion = model.OtherReligion;
                person.Religion = model.Religion;
                person.ContactAddress = model.ContactAddress;
            }
            return person;
        }
    }
}
