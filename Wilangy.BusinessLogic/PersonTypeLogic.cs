﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class PersonTypeLogic:CoreLogic<PersonType>
    {
        IService service;
        public PersonTypeLogic(IService _service) : base(_service)
        {
            service = _service;
        }
    }
}
