﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class StaffLogic : CoreLogic<Staff>
    {
        IService service;
        public StaffLogic(IService _service) : base(_service)
        {
            service = _service;
        }


        public Staff CreateStaff(StaffDTO model, string id)
        {
            Staff staff = AddEntity(new Staff
            {
                StaffId  = id,
                PersonId = id,
                StaffType = model.StaffType,
                GurantorAddress = model.GurantorAddress,
                GurantorName = model.GurantorName,
                GurantorPhone = model.GurantorPhone,
                DateOfEmployment = model.DateOfEmployment == null ? DateTime.Now.Date : model.DateOfEmployment
            });
            return staff;
        }

        public Staff UpdateStaffDetails(StaffDTO model, string id)
        {
            Staff staff = GetEntityBy(s => s.PersonId == id);
            if (staff != null)
            {
                staff.StaffType = model.StaffType;
                staff.GurantorAddress = model.GurantorAddress;
                staff.GurantorName = model.GurantorName;
                staff.DateOfEmployment = model.DateOfEmployment == null ? DateTime.Now.Date : model.DateOfEmployment;
                return staff;
            }
            return null;
        }
    }
}
