﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class StaffTaskLogic : CoreLogic<StaffTask>
    {
        IService _service;
        public StaffTaskLogic(IService service) : base(service)
        {
            _service = service;
        }

    }
}
