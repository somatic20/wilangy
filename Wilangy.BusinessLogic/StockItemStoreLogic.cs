﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class StockItemStoreLogic:CoreLogic<StockItemStore>
    {
        IService service;
        public StockItemStoreLogic(IService _service) : base(_service)
        {
            service = _service;
        }
    }
}
