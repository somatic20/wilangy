﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class StockitemRequestLogic : CoreLogic<StockitemRequest>
    {
        IService _service;
        public StockitemRequestLogic (IService service):base(service)
        {
            _service = service;
        }
    }
}
