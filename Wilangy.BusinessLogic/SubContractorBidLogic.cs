﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class SubContractorBidLogic:CoreLogic<SubContractorBid>
    {
        IService _service;
        public SubContractorBidLogic(IService service):base(service)
        {
            _service = service;
        }
    }
}
