﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class SubContractorLogic:CoreLogic<SubContractor>
    {
        IService _service;
        public SubContractorLogic(IService service):base(service)
        {
            _service = service;
        }
    }
}
