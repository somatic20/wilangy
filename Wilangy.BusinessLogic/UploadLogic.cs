﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class UploadLogic:CoreLogic<Upload>
    {
        IService _service;
        public UploadLogic(IService service) : base(service)
        {
            _service = service;
        }
    }
}
