﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class VehicleLogic:CoreLogic<Vehicle>
    {
        IService service;
        public VehicleLogic(IService _service) : base(_service)
        {
            service = _service;
        }
    }
}
