﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Data;
using Wilangy.Models.Entity;

namespace Wilangy.BusinessLogic
{
    public class VehicleStoreLogic:CoreLogic<VehicleStore>
    {
        IService service;
        public VehicleStoreLogic(IService _service) : base(_service)
        {
            service = _service;
        }
    }
}
