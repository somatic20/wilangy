﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class CorrespondenceDTO
    {
        public int ContractId { get; set; }
        public string Details { get; set; }
        public string EnteredBy { get; set; }
        public List<UploadDTO> Uploads { get; set; }
    }

    public class UpdateCorrespondenceDTO
    {
        public Guid CorrespondenceId { get; set; }
        public string Details { get; set; }
        public string EnteredBy { get; set; }
        public List<UpdateUploadDTO> UpdateUploads { get; set; }

    }
}
