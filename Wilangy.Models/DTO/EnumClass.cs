﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class EnumClass
    {
        public enum Signature
        {
            Signed = 1,
            Unsigned = 2
        }

        public enum Status
        {
            successful = 1,
            unsuccessful = 2
        }

        public enum HttpStatus
        {
            Success = 1,
            Error = 2,
        }

        public enum StaffType
        {
            OfficeStaff = 1,
            FieldStaff = 2,
            AdhocStaff = 3,
            Driver = 4
        }

        public enum Sections
        {
            StockItem = 1,
            Vehicle,
            Staff
        }

        public enum Transaction
        {
            Credit = 1,
            Debit
        }

        public enum StockitemStatus
        {
            Added = 1,
            Removed,
            Approved,
            Rejected
        }



    }
}
