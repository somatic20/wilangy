﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Models.Entity;

namespace Wilangy.Models.DTO
{
    public class LoginData
    {
        public string Id { get; set; }
        public string AuthToken { get; set; }
        public string Fullname { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int ExpirationTime { get; set; }
        public int PersonType { get; set; }
        public string Role { get; set; }
        public string UserId { get; set; }
    }

    public class CompleteLoginData
    {
        public LoginData LoginData { get; set; }
        public Person Person { get; set; }
    }
}
