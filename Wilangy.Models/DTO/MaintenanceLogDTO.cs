﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class MaintenanceLogDTO
    {
        public int VehicleId { get; set; }
        public string Details { get; set; }
        public DateTime DateEntered { get; set; }
        public string EnteredBy { get; set; }
    }

    public class UpdateMaintenanceLogDTO:MaintenanceLogDTO
    {
        public int Id { get; set; }
    }
}
