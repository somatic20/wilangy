﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class PersonDTO
    {
        public string PersonId { get; set; }
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
        public string Othername { get; set; }
        [Required]
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public Nullable<int> GenderId { get; set; }
        public string ContactAddress { get; set; }
        public string StateOfOrigin { get; set; }
        public string Lga { get; set; }
        public string PictureUrl { get; set; }
        [Required]
        public string EnteredBy { get; set; }
        [Required]
        public int PersonTypeId { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Religion { get; set; }
        public string OtherReligion { get; set; }
        [Required]
        public string Role { get; set; }
        public string[] Claims { get; set; }

    }
}
