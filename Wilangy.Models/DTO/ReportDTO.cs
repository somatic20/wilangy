﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class ReportDTO
    {
        public int ContractId { get; set; }
        public string Details { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EnteredBy { get; set; }
        public List<UploadDTO> Uploads { get; set; }
    }

    public class UpdateReportDTO
    {
        public Guid ReportId { get; set; }
        public string Details { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EnteredBy { get; set; }
        public List<UpdateUploadDTO> UpdateUploads { get; set; }
    }
}
