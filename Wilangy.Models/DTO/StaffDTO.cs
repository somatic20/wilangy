﻿using System;

namespace Wilangy.Models.DTO
{
    public class StaffDTO
    {
        public int StaffType { get; set; }
        public string GurantorName { get; set; }
        public string GurantorAddress { get; set; }
        public string GurantorPhone { get; set; }
        public string Religion { get; set; }
        public DateTime? DateOfEmployment { get; set; }
    }
}
