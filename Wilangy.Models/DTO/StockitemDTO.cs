﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class StockitemDTO
    {
        public int StockItemId { get; set; }
        public string ImageUrl { get; set; }
        [Required]
        public string StockItemName { get; set; }
        [Required]
        public int Quantity { get; set; }
        public string StaffId { get; set; }
        public string DetailsAboutStockItem { get; set; }
        public DateTime? DateIn { get; set; }
    }

    public class StockitemRequestDTO
    {
        public int StockitemRequestId { get; set; }
        public string RequestName { get; set; }
        public double EstimatedPrice { get; set; }
        public int NumberOfItems { get; set; }
        public string StoreToPurchaseFrom { get; set; }
        public string StaffMakingRequest { get; set; }
        public string VehicleName { get; set; }
        public int Status { get; set; }
    }
}
