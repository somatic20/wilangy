﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class StockitemStoreDTO
    {
        public int StockItemStoreId { get; set; }
        public int StockItemId { get; set; }
        public string StockitemName { get; set; }
        public DateTime DateOut { get; set; }
        public DateTime? ReturnDate { get; set; }
        public DateTime DateActiviyWasMade { get; set; }
        public int Quantity { get; set; }
        public int NoAdded { get; set; }
        public int? NoReturned { get; set; }
        public int? NoAvailable { get; set; }
        public string MoreDetails { get; set; }
        public string PersonWithStockItem { get; set; }
        public string WhereStockItemIsUsed { get; set; }
        public int Status { get; set; }
        public string StaffId { get; set; }

    }
}
