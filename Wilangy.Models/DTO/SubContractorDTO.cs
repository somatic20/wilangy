﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class SubContractorDTO
    {
        public int SubContractorId { get; set; }
        public string FullName { get; set; }
        public string CompanyName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }

    }

    public class SubContractorBidDTO
    {
        public string Resource { get; set; }
        public int ResourceCount { get; set; }
        public double BidAmount { get; set; }
        public int ContractId { get; set; }
        public int SubContractorId { get; set; }
    }

    public class AllSubContractorDetails
    {
        public SubContractorDTO subContractorDTO { get; set; }
        public SubContractorBidDTO subContractorBidDTO { get; set; }
    }
}
