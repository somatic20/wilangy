﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class UploadDTO
    {
        public string UrlLink { get; set; }
    }

    public class UpdateUploadDTO:UploadDTO
    {
        public Guid UploadId { get; set; }
    }
}
