﻿namespace Wilangy.Models.DTO
{
    public class UserDTO
    {
        public PersonDTO PersonDTO { get; set; }
        public StaffDTO StaffDTO { get; set; }
    }
}
