﻿using Wilangy.Models.Entity;

namespace Wilangy.Models.DTO
{
    public class UserViewModel
    {
        public Person Person { get; set; }
        public Staff Staff { get; set; }
    }
}
