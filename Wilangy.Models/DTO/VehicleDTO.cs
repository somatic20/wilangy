﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class VehicleDTO
    {
        public int VehicleId { get; set; }
        public string ImageUrl { get; set; }
        public string Name { get; set; }
        public string Details { get; set; }
        public string DriverId { get; set; }
        public string DriversName { get; set; }
        public string EnteredBy { get; set; }
        public DateTime? DateIn { get; set; }
        public string PlateNumber { get; set; }

    }
}
