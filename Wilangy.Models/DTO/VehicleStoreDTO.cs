﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.DTO
{
    public class VehicleStoreDTO
    {
        public int VehicleStoreId { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public DateTime DateEntered { get; set; }
        public string StaffId { get; set; }
        public string StaffWithVehicle { get; set; }
        public int TotalCostOfRepairs { get; set; }
        public string Location { get; set; }
        public string Details { get; set; }
        public List<stockitemList> StockitemList { get; set; }
    }

    public class stockitemList
    {
        public string StockItemName { get; set; }
        public int Quantity { get; set; }

    }
}
