﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Account
    {
        public int AccountId { get; set; }
        public int Transaction { get; set; }
        public double Amount { get; set; }
        public DateTime DateEntered { get; set; }
        public string Description { get; set; }
        public string EnteredBy { get; set; }
        public int SectionType { get; set; }
        public string ItemName { get; set; }
    }
}
