﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class AllStaffView
    {
        public string Id { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Othername { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public Nullable<DateTime> DateOfBirth { get; set; }
        public int GenderId { get; set; }
        public string ContactAddress { get; set; }
        public string StateOfOrigin { get; set; }
        public string Lga { get; set; }
        public string PictureUrl { get; set; }
        public string EnteredBy { get; set; }
        public int PersonTypeId { get; set; }
        public string Religion { get; set; }
        public string OtherReligion { get; set; }
        public string Fullname
        {
            get
            {
                return Lastname + " " + Firstname + " " + Othername;
            }
        }
        public string StaffId { get; set; }
        public string PersonId { get; set; }
        public int StaffType { get; set; }
        public string GurantorName { get; set; }
        public string GurantorAddress { get; set; }
        public string GurantorPhone { get; set; }
        public DateTime? DateOfEmployment { get; set; }
    }
}
