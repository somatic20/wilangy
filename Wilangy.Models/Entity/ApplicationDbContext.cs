﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class ApplicationDbContext : IdentityDbContext<Person>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

        public DbSet<Contract> Contract { get; set; }
        public DbSet<Gender> Gender { get; set; }
        public DbSet<StockItem> StockItem { get; set; }
        public DbSet<StockItemStore> StockItemStore { get; set; }
        public DbSet<Vehicle> Vehicle { get; set; }
        public DbSet<VehicleStore> VehicleStore { get; set; }
        public DbSet<VehicleStoreStockItem> VehicleStoreStockItem { get; set; }
        public DbSet<LoginLog> LoginLog { get; set; }
        public DbSet<Account> Account { get; set; }
        public DbSet<SubContractor> SubContractor { get; set; }
        public DbSet<SubContractorBid> SubContractorBid { get; set; }
        public DbSet<StockitemRequest> StockitemRequest { get; set; }
        public DbSet<Upload> Uploads { get; set; }
        public DbSet<Report> Reports { get; set; }
        public DbSet<Correspondence> Correspondences { get; set; }
        public DbSet<StaffTask> StaffTasks { get; set; }
        public DbSet<MaintenanceLog> MaintenanceLogs { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Query<AllStaffView>().ToView("VW_ALL_STAFF");
            builder.Entity<Contract>().HasAlternateKey(p => p.ContractName);
        }

    }
}
