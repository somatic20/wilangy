﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Contract
    {
        public int ContractId { get; set; }
        public string ContractName { get; set; }
        public string StaffId { get; set; }
        public string SiteName { get; set; }
        public string AwardLetter { get; set; }
        public string BankStatement { get; set; }
        public string BankApg { get; set; }
        public string Certificate { get; set; }
        public string ForwardingLetter { get; set; }
        public string Beme { get; set; }
        public string AcceptanceLetter { get; set; }
        public string MaterialTesting { get; set; }
        public string ResdientEngineer { get; set; }
        public string Location { get; set; }
        public DateTime DateEntered { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
