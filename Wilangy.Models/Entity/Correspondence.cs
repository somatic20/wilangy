﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Correspondence
    {
        public Guid CorrespondenceId { get; set; }
        public int ContractId { get; set; }
        public string Details { get; set; }
        public string EnteredBy { get; set; }
        public DateTime DateEntered { get; set; }

        [ForeignKey("ContractId")]
        public virtual Contract Contract { get; set; }
        public virtual ICollection<Upload> Uploads { get; set; }
    }
}
