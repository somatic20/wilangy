﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Gender
    {
        public int GenderId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Person> Persons { get; set; }
    }
}
