﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class LoginLog
    {
        public string LoginLogID { get; set; }
        public string UserID { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime LogOutTime { get; set; }

        [ForeignKey("UserID")]
        public Person Person { get; set; }
    }
}
