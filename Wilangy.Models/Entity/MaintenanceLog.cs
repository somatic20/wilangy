﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class MaintenanceLog
    {
        public int MaintenanceLogId { get; set; }
        public int VehicleId { get; set; }
        public string Details { get; set; }
        public string EnteredBy { get; set; }
        public DateTime DateEntered { get; set; }

        [ForeignKey("VehicleId")]
        public Vehicle Vehicle { get; set; }
    }
}
