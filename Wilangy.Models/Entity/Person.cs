﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Person : IdentityUser
    {
        [Required]
        public string Firstname { get; set; }
        [Required]
        public string Lastname { get; set; }
        public string Othername { get; set; }
        public Nullable<DateTime> DateOfBirth { get; set; }
        public int? GenderId { get; set; }
        public string ContactAddress { get; set; }
        public string StateOfOrigin { get; set; }
        public string Lga { get; set; }
        public string PictureUrl { get; set; }
        [Required]
        public string EnteredBy { get; set; }
        public int PersonTypeId { get; set; }
        public string Religion { get; set; }
        public string OtherReligion { get; set; }
        public string Fullname
        {
            get
            {
                return Lastname + " " + Firstname + " " + Othername;
            }
        }
        public string FCMToken { get; set; }
        public virtual Gender Gender { get; set; }
        public virtual PersonType PersonType { get; set; }
        //public virtual Staff Staff { get; set; }
    }
}
