﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class PersonType
    {
        public int PersonTypeId { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Person> Persons { get; set; }
    }
}
