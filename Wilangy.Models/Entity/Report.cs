﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Report
    {
        public Guid ReportId { get; set; }
        public int ContractId { get; set; }
        public string Details { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string EnteredBy { get; set; }
        public DateTime DateEntered { get; set; }
        public virtual ICollection<Upload> Uploads { get; set; }
    }
}
