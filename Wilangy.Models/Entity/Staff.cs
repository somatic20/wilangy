﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Staff
    {
        public string StaffId { get; set; }
        public string PersonId { get; set; }
        public int StaffType { get; set; }
        public string GurantorName { get; set; }
        public string GurantorAddress { get; set; }
        public string GurantorPhone { get; set; }
        public DateTime? DateOfEmployment { get; set; }

        //[ForeignKey("PersonId")]
        //public virtual Person Person { get; set; }
    }
}
