﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class StaffTask
    {
        public int StaffTaskId { get; set; }
        public string StaffId { get; set; }
        public string StaffName { get; set; }
        public string Details { get; set; }
        public string UploadLink { get; set; }
        public string EnteredBy { get; set; }
        public DateTime DateEntered { get; set; }
        public bool Completed { get; set; }
    }
}
