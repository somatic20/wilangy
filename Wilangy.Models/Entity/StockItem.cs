﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class StockItem
    {
        public int StockItemId { get; set; }
        public string StockItemName { get; set; }
        public int Quantity { get; set; }
        public string ImageUrl { get; set; }
        public DateTime? DateIn { get; set; }
        public string StaffId { get; set; }
        public string Details { get; set; }

        public virtual Staff Staff { get; set; }
    }
}
