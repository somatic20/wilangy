﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class StockitemRequest
    {
        public int StockitemRequestId { get; set; }
        public string RequestName { get; set; }
        public double EstimatedPrice { get; set; }
        public int NumberOfItems { get; set; }
        public string StoreToPurchaseFrom { get; set; }
        public string StaffMakingRequest { get; set; }
        public string VehicleName {get; set; }
        public int? Status { get; set; }
    }
}
