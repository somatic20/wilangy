﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class SubContractorBid
    {
        public int SubContractorBidId { get; set; }
        public string Resource { get; set; }
        public int ResourceCount { get; set; }
        public double BidAmount { get; set; }
        public int ContractId { get; set; }
        public int SubContractorId { get; set; }
        public SubContractor SubContractor { get; set; }
        public Contract Contract { get; set; }
    }
}
