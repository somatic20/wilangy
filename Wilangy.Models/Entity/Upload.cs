﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class Upload
    {
        public Guid UploadId { get; set; }
        public Guid ItemId { get; set; }
        public string UrlLink { get; set; }
    }
}
