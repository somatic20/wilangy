﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class VehicleStore
    {
        public int VehicleStoreId { get; set; }
        public int VehicleId { get; set; }
        public string VehicleName { get; set; }
        public string StockItemName { get; set; }
        public int Quantity { get; set; }
        public DateTime DateEntered { get; set; }
        public string StaffId { get; set; }
        public string StaffWithVehicle { get; set; }
        public int TotalCostOfRepairs { get; set; }
        public string RepairDetails { get; set; }
        public string Location { get; set; }

        public virtual Staff Staff { get; set; }
        public virtual Vehicle Vehicle { get; set; }
    }
}
