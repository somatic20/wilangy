﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wilangy.Models.Entity
{
    public class VehicleStoreStockItem
    {
        public int VehicleStoreStockitemId { get; set; }
        public int StockitemId { get; set; }
        public virtual StockItem StockItem { get; set; }
    }
}
