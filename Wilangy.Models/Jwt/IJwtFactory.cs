﻿using System.Security.Claims;
using System.Threading.Tasks;
using Wilangy.Models.Entity;

namespace Object.Jwt
{
    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(Person user, string personType);
        ClaimsIdentity GenerateClaimsIdentity(string userName, string id);
    }
}
