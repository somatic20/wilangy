﻿using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Wilangy.Models.Entity;

namespace Object.Jwt
{
    public class Tokens
    {
        public static async Task<jwtObject> GenerateJwt(ClaimsIdentity identity, IJwtFactory jwtFactory, Person user, string personType, JwtIssuerOptions jwtOptions)
        {
            jwtObject response = new jwtObject()
            {
                id = identity.Claims.Single(c => c.Type == "id").Value,
                auth_token = await jwtFactory.GenerateEncodedToken(user, personType),
                expires_in = (int)jwtOptions.ValidFor.Hours
            };

            return response;
        }
    }
    public class jwtObject
    {
        public string id { get; set; }
        public string auth_token { get; set; }
        public int expires_in { get; set; }
        
    }
    
}
