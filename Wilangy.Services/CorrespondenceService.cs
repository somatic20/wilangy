﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Services
{
    public class CorrespondenceService : ICorrespondenceService
    {
        CorrespondenceLogic _correspondenceLogic { get; }
        ContractLogic _contractLogic { get; }
        IUploadService _uploadService { get; }
        IService _service { get; }

        public CorrespondenceService(CorrespondenceLogic correspondenceLogic, ContractLogic contractLogic, IUploadService uploadService, IService service)
        {
            _correspondenceLogic = correspondenceLogic;
            _contractLogic = contractLogic;
            _uploadService = uploadService;
            _service = service;
        }

        public Correspondence Add(CorrespondenceDTO dto)
        {
            Contract contract = _contractLogic.GetEntityBy(p => p.ContractId == dto.ContractId);
            if (contract == null)
                throw new InvalidOperationException("Contract not found");
            Correspondence correspondence = _correspondenceLogic.AddEntity(new Correspondence()
            {
                ContractId = contract.ContractId,
                Details = dto.Details,
                DateEntered = DateTime.Now,
                EnteredBy = dto.EnteredBy
            });
            _service.Save();

            bool uploaded = _uploadService.Add(dto.Uploads, correspondence.CorrespondenceId);
            if (!uploaded)
                throw new InvalidOperationException("Upload failed");
            return correspondence;
        }

        public Correspondence Update(UpdateCorrespondenceDTO update)
        {
            Correspondence correspondence = _correspondenceLogic.GetEntityBy(p => p.CorrespondenceId == update.CorrespondenceId);
            if (correspondence == null)
                throw new InvalidOperationException("Correspondence entity not found");
            correspondence.Details = update.Details;
            correspondence.EnteredBy = update.EnteredBy;

            if (update.UpdateUploads.Count > 0)
            {
                bool value = _uploadService.Update(update.UpdateUploads);
                if (!value)
                    throw new InvalidOperationException("update for correspondence upload failed");
            }

            _service.Save();
            return correspondence;
        }

        public List<Correspondence> GetCorrespondencesForContract(int contractId)
        {
            return _correspondenceLogic.GetEntitiesBy(p => p.ContractId == contractId);
        }
    }
}
