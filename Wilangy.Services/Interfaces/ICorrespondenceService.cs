﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.Services.Interfaces
{
    public interface ICorrespondenceService
    {
        Correspondence Add(CorrespondenceDTO dto);
        Correspondence Update(UpdateCorrespondenceDTO update);
        List<Correspondence> GetCorrespondencesForContract(int contractId);
    }
}
