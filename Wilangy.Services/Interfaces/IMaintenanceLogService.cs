﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.Services.Interfaces
{
    public interface IMaintenanceLogService
    {
        MaintenanceLog AddLog(MaintenanceLogDTO model);
        MaintenanceLog UpdateLog(UpdateMaintenanceLogDTO model);
        List<MaintenanceLog> GetMaintenanceLogs();
        List<MaintenanceLog> GetMaintenanceLogsByVehicleId(int id);
    }
}
