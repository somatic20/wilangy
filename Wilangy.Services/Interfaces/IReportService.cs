﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.Services.Interfaces
{
    public interface IReportService
    {
        Report Add(ReportDTO model);
        Report Update(UpdateReportDTO update);
        List<Report> GetContractReports(int id);
    }
}
