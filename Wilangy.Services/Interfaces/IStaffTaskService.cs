﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.Services.Interfaces
{
    public interface IStaffTaskService
    {
        StaffTask AddStaffTask(StaffTaskDto model);
        StaffTask UpdateStaffTask(UpdateStaffTaskDto model);
        List<StaffTask> GetAllTasks();
        List<StaffTask> GetAllTasksById(string id);
    }
}
