﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.Services.Interfaces
{
    public interface IUploadService
    {
        bool Add(List<UploadDTO> model, Guid id);
        List<Upload> GetUpload(Guid id);
        bool Update(List<UpdateUploadDTO> uploads);
    }
}
