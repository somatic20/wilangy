﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Services
{
    public class MaintenanceLogService : IMaintenanceLogService
    {
        IService _service { get; }
        MaintenanceLogLogic _maintenanceLogLogic { get; }
        VehicleLogic _vehicleLogic { get; }

        public MaintenanceLogService(IService service, MaintenanceLogLogic maintenanceLogLogic, VehicleLogic vehicleLogic)
        {
            _service = service;
            _maintenanceLogLogic = maintenanceLogLogic;
            _vehicleLogic = vehicleLogic;
        }

        public MaintenanceLog AddLog(MaintenanceLogDTO model)
        {
            Vehicle vehicle = _vehicleLogic.GetEntityBy(p => p.VehicleId == model.VehicleId);
            if (vehicle == null)
                throw new InvalidOperationException("Vehicle not found");
            MaintenanceLog log = new MaintenanceLog()
            {
                Details = model.Details,
                VehicleId = model.VehicleId,
                DateEntered = model.DateEntered == null ? DateTime.Now : model.DateEntered,
                EnteredBy = model.EnteredBy
            };
            _maintenanceLogLogic.AddEntity(log);
            _service.Save();
            return log;
        }

        public MaintenanceLog UpdateLog(UpdateMaintenanceLogDTO model)
        {
            MaintenanceLog log = _maintenanceLogLogic.GetEntityBy(p => p.MaintenanceLogId == model.Id);
            if (log == null)
                throw new InvalidOperationException("Log not found");
            log.Details = model.Details;
            _service.Save();
            return log;
        }

        public List<MaintenanceLog> GetMaintenanceLogs()
        {
            return _maintenanceLogLogic.GetAllEntities();
        }
        public List<MaintenanceLog> GetMaintenanceLogsByVehicleId(int id)
        {
            return _maintenanceLogLogic.GetEntitiesBy(p => p.VehicleId == id);
        }
    }
}
