﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Services
{
    public class ReportService : IReportService
    {
        ReportLogic _reportLogic { get; }
        ContractLogic _contractLogic { get; }
        IUploadService _uploadService { get; }
        IService _service { get; }
        public ReportService(ReportLogic reportLogic, ContractLogic contractLogic, IUploadService uploadService, IService service)
        {
            _reportLogic = reportLogic;
            _contractLogic = contractLogic;
            _uploadService = uploadService;
            _service = service;
        }


        public Report Add(ReportDTO model)
        {
            Contract contract = _contractLogic.GetEntityBy(p => p.ContractId == model.ContractId);
            if (contract == null)
                throw new InvalidOperationException("Contract not found");


            Report report = new Report()
            {
                ContractId = model.ContractId,
                Details = model.Details,
                EndDate = model.EndDate,
                StartDate = model.StartDate,
                EnteredBy = model.EnteredBy,
                DateEntered = DateTime.Now
            };
            report = _reportLogic.AddEntity(report);
            bool uploaded = _uploadService.Add(model.Uploads, report.ReportId);
            if (!uploaded)
                throw new InvalidOperationException("Upload failed");
            _service.Save();
            return report;
        }


        public Report Update(UpdateReportDTO update)
        {
            Report report = _reportLogic.GetEntityBy(p => p.ReportId == update.ReportId);
            if (report == null)
                throw new InvalidOperationException("Report not found");
            report.Details = update.Details;
            report.EndDate = update.EndDate;
            report.StartDate = update.StartDate;
            report.EnteredBy = update.EnteredBy;
            if (update.UpdateUploads.Count > 0)
            {
                bool value = _uploadService.Update(update.UpdateUploads);
                if (!value)
                    throw new InvalidOperationException("update for report upload failed");
            }
            _service.Save();
            return report;
        }

        public List<Report> GetContractReports(int id)
        {
            return _reportLogic.GetEntitiesBy(p => p.ContractId == id);
        }
    }
}
