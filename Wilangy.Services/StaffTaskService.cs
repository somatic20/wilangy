﻿using System;
using System.Collections.Generic;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Services
{
    public class StaffTaskService : IStaffTaskService
    {
        StaffTaskLogic _staffTaskLogic { get; }
        StaffLogic _staffLogic { get; }
        IService _service { get; }
        public StaffTaskService(IService service, StaffLogic staffLogic, StaffTaskLogic staffTaskLogic)
        {
            _staffLogic = staffLogic;
            _staffTaskLogic = staffTaskLogic;
            _service = service;
        }



        public StaffTask AddStaffTask(StaffTaskDto model)
        {
            Staff staff = _staffLogic.GetEntityBy(p => p.StaffId == model.StaffId);

            StaffTask staffTask = _staffTaskLogic.AddEntity(new StaffTask()
            {
                DateEntered = model.DateEntered,
                Details = model.Details,
                EnteredBy = model.EnteredBy,
                StaffId = model.StaffId,
                StaffName = model.StaffName,
                UploadLink = model.UploadLink
            });
            _service.Save();
            return staffTask;
        }


        public StaffTask UpdateStaffTask(UpdateStaffTaskDto model)
        {
            StaffTask staffTask = _staffTaskLogic.GetEntityBy(p => p.StaffTaskId == model.StaffTaskId);
            if (staffTask == null)
                throw new InvalidOperationException("Task not found");
            staffTask.Details = model.Details;
            staffTask.EnteredBy = model.EnteredBy;
            staffTask.StaffId = model.StaffId;
            staffTask.StaffName = model.StaffName;
            staffTask.UploadLink = model.UploadLink;
            staffTask.Completed = model.Completed;
            _service.Save();
            return staffTask;
        }

        public List<StaffTask> GetAllTasksById(string id)
        {
            return _staffTaskLogic.GetEntitiesBy(p => p.StaffId == id);
        }

        public List<StaffTask> GetAllTasks()
        {
            return _staffTaskLogic.GetAllEntities();
        }

    }
}
