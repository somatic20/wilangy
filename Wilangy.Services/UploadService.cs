﻿using System;
using System.Collections.Generic;
using System.Text;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Services
{
    public class UploadService : IUploadService
    {
        UploadLogic _uploadLogic { get; }
        IService _service { get; }

        public UploadService(UploadLogic uploadLogic, IService service)
        {
            _uploadLogic = uploadLogic;
            _service = service;
        }

        public bool Add(List<UploadDTO> model, Guid id)
        {
            try
            {
                foreach (UploadDTO item in model)
                {
                    Upload upload = new Upload()
                    {
                        UrlLink = item.UrlLink,
                        ItemId = id
                    };
                    _uploadLogic.AddEntity(upload);
                }
                _service.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool Update(List<UpdateUploadDTO> uploads)
        {
            try
            {
                foreach (UpdateUploadDTO item in uploads)
                {
                    Upload upload = _uploadLogic.GetEntityBy(p => p.UploadId == item.UploadId);
                    if (upload == null)
                        continue;
                    upload.UrlLink = item.UrlLink;
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<Upload> GetUpload(Guid id)
        {
            return _uploadLogic.GetEntitiesBy(p => p.ItemId == id);
        }
    }
}
