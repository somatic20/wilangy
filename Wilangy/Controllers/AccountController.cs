﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    public class AccountController : BaseAPIController
    {
        AccountLogic _accountLogic;
        IService _service;

        public AccountController(IService service, AccountLogic accountLogic)
        {
            _accountLogic = accountLogic;
            _service = service;
        }

        [HttpPost("AddAccount")]
        public IActionResult AddAccount(AccountDTO model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    Account account = new Account
                    {
                        Amount = model.Amount,
                        DateEntered = model.DateEntered,
                        Description = model.Description,
                        EnteredBy = model.EnteredBy,
                        SectionType = model.SectionType,
                        ItemName = model.ItemName,
                        Transaction = model.Transaction
                    };
                    _accountLogic.AddEntity(account);
                    int count = _service.Save();
                    return Ok(account, (int)EnumClass.Status.successful, "Added account successfully", true);
                }
                return BadRequest(null, (int)EnumClass.Status.successful, "couldn't add account", false);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("AllAccountDetails")]
        public IActionResult AllAccounts()
        {
            List<Account> accounts = _accountLogic.GetAllEntities();
            return Ok(accounts);
        }

        [HttpGet("GetAccountsByTransction")]
        public IActionResult GetAccountByTransaction(int id)
        {
            List<Account> accounts = _accountLogic.GetEntitiesBy(p => p.Transaction == id);
            return Ok(accounts);
        }

    }
}