﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    public class ContractController : BaseAPIController
    {
        IService service;
        ContractLogic ContractLogic;
        SubContractorLogic _subContractorLogic;
        SubContractorBidLogic _subContractorBidLogic;

        readonly IMapper Mapper;
        public ContractController(IService _service, ContractLogic contractLogic, IMapper mapper, SubContractorBidLogic subContractorBidLogic, SubContractorLogic subContractorLogic)
        {
            service = _service;
            ContractLogic = contractLogic;
            Mapper = mapper;
            _subContractorBidLogic = subContractorBidLogic;
            _subContractorLogic = subContractorLogic;

        }

        [HttpPost("CreateContract")]
        public IActionResult CreateContract(ContractDTO model)
        {
            Contract check = ContractLogic.GetEntityBy(c => c.ContractName == model.ContractName);
            if (check == null)
            {
                Contract contract = new Contract
                {
                    ContractName = model.ContractName,
                    AcceptanceLetter = model.AcceptanceLetter,
                    AwardLetter = model.AwardLetter,
                    BankApg = model.BankApg,
                    BankStatement = model.BankStatement,
                    Beme = model.Beme,
                    Certificate = model.Certificate,
                    ForwardingLetter = model.ForwardingLetter,
                    MaterialTesting = model.MaterialTesting,
                    ResdientEngineer = model.ResdientEngineer,
                    SiteName = model.SiteName,
                    StaffId = model.StaffId,
                    Location = model.Location,
                    DateEntered = model.DateEntered != null ? model.DateEntered : DateTime.Now.Date
                };
                ContractLogic.AddEntity(contract);
                int count = ContractLogic.Save();

                return Ok(contract, (int)EnumClass.HttpStatus.Success, "success", true);
            }
            return BadRequest(null, (int)EnumClass.HttpStatus.Error, "failed", false);
        }

        [HttpPut("UpdateContractDetails")]
        public IActionResult UpdateContractDetails(ContractDTO model)
        {
            Contract contract = ContractLogic.GetEntityBy(c => c.ContractId == model.ContractId);
            if (contract != null)
            {
                contract.ContractName = model.ContractName;
                contract.AcceptanceLetter = model.AcceptanceLetter;
                contract.AwardLetter = model.AwardLetter;
                contract.BankApg = model.BankApg;
                contract.BankStatement = model.BankStatement;
                contract.Beme = model.Beme;
                contract.Certificate = model.Certificate;
                contract.ForwardingLetter = model.ForwardingLetter;
                contract.MaterialTesting = model.MaterialTesting;
                contract.ResdientEngineer = model.ResdientEngineer;
                contract.SiteName = model.SiteName;
                contract.DateEntered = model.DateEntered;

                int count = ContractLogic.Save();

                return Ok(contract, (int)EnumClass.HttpStatus.Success, "success", true);
            }
            return BadRequest(null, (int)EnumClass.HttpStatus.Error, "notfound", false);
        }

        [HttpGet("GetAllContracts")]
        public IActionResult GetAllContracts()
        {
            try
            {
                List<Contract> contracts = ContractLogic.GetAllEntities();

                var data = Mapper.Map<List<ContractDTO>>(contracts);
                return Ok(data, (int)EnumClass.HttpStatus.Success, "allContracts", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpPost("AddSubContractorToContract")]
        public IActionResult AddSubContractor([FromBody]AllSubContractorDetails viewmodel)
        {
            try
            {
                SubContractor subContractor = new SubContractor
                {
                    Address = viewmodel.subContractorDTO.Address,
                    CompanyName = viewmodel.subContractorDTO.CompanyName,
                    FullName = viewmodel.subContractorDTO.FullName,
                    PhoneNo = viewmodel.subContractorDTO.PhoneNo,
                };
                _subContractorLogic.AddEntity(subContractor);
                int count = _subContractorLogic.Save();

                if (viewmodel.subContractorBidDTO.Resource != null)
                {
                    SubContractorBid subContractorBid = new SubContractorBid
                    {
                        SubContractorId = subContractor.SubContractorId,
                        BidAmount = viewmodel.subContractorBidDTO.BidAmount
                    };
                    _subContractorBidLogic.AddEntity(subContractorBid);
                    int num = _subContractorBidLogic.Save();
                }


                return Ok(viewmodel, (int)EnumClass.HttpStatus.Success, "success", true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

    }
}
