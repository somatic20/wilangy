﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CorrespondenceController : BaseAPIController
    {
        ICorrespondenceService _correspondenceService { get; }
        public CorrespondenceController(ICorrespondenceService correspondenceService)
        {
            _correspondenceService = correspondenceService;
        }
        [HttpPost("[action]")]
        public IActionResult Add(CorrespondenceDTO model)
        {
            try
            {
                Correspondence correspondence = _correspondenceService.Add(model);
                return Ok(correspondence, (int)EnumClass.HttpStatus.Success, "Success", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(UpdateCorrespondenceDTO model)
        {
            try
            {
                Correspondence correspondence = _correspondenceService.Update(model);
                return Ok(correspondence, (int)EnumClass.HttpStatus.Success, "Success", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }


        [HttpGet("[action]/contractId")]
        public IActionResult GetCorrespondencesForContract(int contractId)
        {
            try
            {
                List<Correspondence> correspondences = _correspondenceService.GetCorrespondencesForContract(contractId);
                return Ok(correspondences, (int)EnumClass.HttpStatus.Success, "Success", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }
    }
}
