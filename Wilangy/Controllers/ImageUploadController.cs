﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wilangy.Api.models;
using Wilangy.Models.DTO;
using static Wilangy.Models.Constants;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    public class ImageUploadController : Controller
    {
        IHostingEnvironment hostingEnvironment;

        public ImageUploadController(IHostingEnvironment _hostingEnvironment)
        {
            hostingEnvironment = _hostingEnvironment;
        }


        [HttpPost("UploadImage")]
        public MessageResponse<string> UploadImage(IFormFile file)
        {
            try
            {
                if (file.Length > 0)
                {
                    var fileName = Path.GetFileName(file.FileName);
                    string extension = Path.GetExtension(file.FileName).ToLower();
                    string rejectReason = InvalidFile(file.Length, extension);
                    if (string.IsNullOrEmpty(rejectReason))
                    {
                        string passportUrl = UploadWithCloudinary(file).Result.Item2;

                        return new MessageResponse<string>()
                        {
                            Data = passportUrl,
                            IsSuccessful = true,
                            Status = (int)EnumClass.Status.successful
                        };

                    }
                    else
                    {
                        return new MessageResponse<string>()
                        {
                            IsSuccessful = false,
                            Message = "upload failed",
                            Status = (int)EnumClass.Status.unsuccessful,
                        };
                    }

                }
                else
                {
                    return new MessageResponse<string>()
                    {
                        IsSuccessful = false,
                        Message = "upload failed",
                        Status = (int)EnumClass.Status.unsuccessful,
                    };

                }
            }
            catch (Exception ex)
            {
                return new MessageResponse<string>()
                {
                    IsSuccessful = false,
                    Message = ex.Message,
                    Status = (int)EnumClass.Status.unsuccessful,
                };
            }

        }
        [NonAction]
        public async Task<(bool, string)> UploadWithCloudinary(IFormFile file)
        {
            Account account = new Account(CloudinaryConfiguration.CLOUD_NAME, CloudinaryConfiguration.API_KEY, CloudinaryConfiguration.API_SECRET);
            Cloudinary cloudinary = new Cloudinary(account);
            string filePath = await GetImageURL(file);
            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(filePath),
                PublicId = Guid.NewGuid().ToString(),
                Tags = "wilangy"

            };
            ImageUploadResult uploadResult = await cloudinary.UploadAsync(uploadParams);
            if (uploadResult.StatusCode == System.Net.HttpStatusCode.Accepted || uploadResult.StatusCode == System.Net.HttpStatusCode.OK)
            {
                return (true, uploadResult.SecureUri.ToString());
            }
            else
            {
                return (true, null);
            }
        }
        [NonAction]
        private async Task<string> GetImageURL(IFormFile file)
        {
            if (file != null)
            {
                //Saving image to a folder and saving its url to db
                string[] allowedFIleExtensions = new[] { ".jpg", ".png", ".jpeg", ".Jpeg", ".docx" };
                string webRootPath = hostingEnvironment.WebRootPath;
                string contentRootPath = hostingEnvironment.ContentRootPath;

                string filenameWithExtension = Path.GetFileName(file.FileName); // getting filename
                string extension = Path.GetExtension(file.FileName).ToLower(); // getting only the extension
                if (allowedFIleExtensions.Contains(extension)) // check extension type
                {
                    string FileNameWithoutExtension = Path.GetFileNameWithoutExtension(filenameWithExtension); //retireves filename without extension
                    FileNameWithoutExtension = FileNameWithoutExtension + DateTime.Now.Millisecond;
                    string FileNameInServer = FileNameWithoutExtension + extension; // add reg number after underscore to make the filename unique for each person
                    string pathToFileInServer = Path.Combine(webRootPath, "Content/Uploads/Images/", FileNameInServer);
                    string passportUrl = "Content/Uploads/Images/" + FileNameInServer;
                    using (var stream = new FileStream(pathToFileInServer, FileMode.Create))
                    {
                        await file.CopyToAsync(stream);
                    }
                    return pathToFileInServer;
                }
            }
            return "";


        }
        [NonAction]
        private string InvalidFile(decimal uploadedFileSize, string fileExtension)
        {
            try
            {
                string message = null;
                decimal oneKiloByte = 1024;
                decimal maximumFileSize = 100 * oneKiloByte;

                decimal actualFileSizeToUpload = Math.Round(uploadedFileSize / oneKiloByte, 1);
                if (InvalidFileType(fileExtension))
                {
                    message = "File type '" + fileExtension + "' is invalid! File type must be any of the following: .jpg, .jpeg, .png or .jif ";
                }
                else if (actualFileSizeToUpload > (maximumFileSize / oneKiloByte))
                {
                    message = "Your file size of " + actualFileSizeToUpload.ToString("0.#") + " Kb is too large, maximum allowed size is " + (maximumFileSize / oneKiloByte) + " Kb";
                }

                return message;
            }
            catch (Exception)
            {
                throw;
            }
        }


        [NonAction]
        private bool InvalidFileType(string extension)
        {
            extension = extension.ToLower();
            switch (extension)
            {
                case ".jpg":
                    return false;
                case ".png":
                    return false;
                case ".gif":
                    return false;
                case ".jpeg":
                    return false;
                case ".docx":
                    return false;
                default:
                    return true;
            }
        }
    }
}
