﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MaintenanceLogController : BaseAPIController
    {
        IMaintenanceLogService _maintenanceLogService { get; }

        public MaintenanceLogController(IMaintenanceLogService maintenanceLogService)
        {
            _maintenanceLogService = maintenanceLogService;
        }

        [HttpPost("[action]")]
        public IActionResult AddLog(MaintenanceLogDTO model)
        {
            try
            {
                MaintenanceLog log = _maintenanceLogService.AddLog(model);
                return Ok(log, (int)EnumClass.HttpStatus.Success, "Success", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpPost("[action]")]
        public IActionResult UpdateLog(UpdateMaintenanceLogDTO model)
        {
            try
            {
                MaintenanceLog log = _maintenanceLogService.UpdateLog(model);
                return Ok(log, (int)EnumClass.HttpStatus.Success, "Updated", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAllLogs()
        {
            try
            {
                List<MaintenanceLog> logs = _maintenanceLogService.GetMaintenanceLogs();
                return Ok(logs);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpGet("[action]/id")]
        public IActionResult GetAllLogsForVehicle(int id)
        {
            try
            {
                List<MaintenanceLog> logs = _maintenanceLogService.GetMaintenanceLogsByVehicleId(id);
                return Ok(logs);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

    }
}
