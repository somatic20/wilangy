﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : BaseAPIController
    {
        IReportService _reportService { get; }
        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [HttpPost("[action]")]
        public IActionResult Add(ReportDTO dto)
        {
            try
            {
                Report report = _reportService.Add(dto);
                return Ok(report, (int)EnumClass.HttpStatus.Success, "Successful", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpPost("[action]")]
        public IActionResult Update(UpdateReportDTO dto)
        {
            try
            {
                Report report = _reportService.Update(dto);
                return Ok(report, (int)EnumClass.HttpStatus.Success, "Updated", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }


        [HttpGet("[action]/contractId")]
        public IActionResult GetContractReports(int contractId)
        {
            try
            {
                List<Report> reports = _reportService.GetContractReports(contractId);
                return Ok(reports);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }
    }
}
