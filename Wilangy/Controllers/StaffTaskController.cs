﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using Wilangy.Services.Interfaces;

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StaffTaskController : BaseAPIController
    {
        IStaffTaskService _staffTaskService { get; }
        public StaffTaskController(IStaffTaskService staffTaskService)
        {
            _staffTaskService = staffTaskService;
        }

        [HttpPost("[action]")]
        public IActionResult AddTask([FromBody] StaffTaskDto staffTaskDto)
        {
            try
            {
                StaffTask task = _staffTaskService.AddStaffTask(staffTaskDto);
                return Ok(task, (int)EnumClass.HttpStatus.Success, "Success", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpPost("[action]")]
        public IActionResult UpdateTask([FromBody] UpdateStaffTaskDto updateTask)
        {
            try
            {
                StaffTask staffTask = _staffTaskService.UpdateStaffTask(updateTask);
                return Ok(staffTask, (int)EnumClass.HttpStatus.Success, "Updated", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpGet("[action]")]
        public IActionResult GetAllTasks()
        {
            try
            {
                List<StaffTask> staffTasks = _staffTaskService.GetAllTasks();
                return Ok(staffTasks);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpGet("[action]/id")]
        public IActionResult GetAllTasks(string id)
        {
            try
            {
                List<StaffTask> staffTasks = _staffTaskService.GetAllTasksById(id);
                return Ok(staffTasks);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }
    }
}
