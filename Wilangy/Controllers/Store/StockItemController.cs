﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Wilangy.Api.models;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers.Store
{
    [Route("api/[controller]")]
    public class StockItemController : BaseAPIController
    {
        readonly IService service;
        readonly StockItemLogic StockItemLogic;
        readonly IMapper Mapper;
        StaffLogic _staffLogic;

        public StockItemController(
        StaffLogic staffLogic , IService _service, StockItemLogic _stockItemLogic, IMapper mapper)
        {
            _staffLogic = staffLogic;
            service = _service;
            StockItemLogic = _stockItemLogic;
            Mapper = mapper;
        }


        [HttpPost("AddStockItem")]
        public IActionResult AddStockItem(StockitemDTO model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    StockItem stockItem = StockItemLogic.GetEntityBy(s => s.StockItemName == model.StockItemName);
                    Staff staff = _staffLogic.GetEntityBy(p => p.StaffId == model.StaffId);
                    if (staff == null)
                    {
                        return BadRequest(null, (int)EnumClass.HttpStatus.Error, "Staff not found", false);
                    }
                    if (stockItem == null)
                    {
                        stockItem = new StockItem
                        {
                            StockItemName = model.StockItemName,
                            DateIn = model.DateIn == null ? DateTime.Now.Date : model.DateIn,
                            Details = model.DetailsAboutStockItem,
                            Quantity = model.Quantity,
                            StaffId = model.StaffId,
                            ImageUrl = model.ImageUrl
                        };
                        StockItemLogic.AddEntity(stockItem);
                        service.Save();
                        var data = Mapper.Map<StockitemDTO>(stockItem);
                        return Ok(data, (int)EnumClass.Status.successful, "StockitemRegistrationSucceded", true);
                    }
                    return BadRequest(null, (int)EnumClass.Status.unsuccessful, "AlreadyRegistered", false);
                }
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, "ModelStateNotValid", false);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpPut("UpdateStockItem")]
        public IActionResult UpdatetockItem(StockitemDTO model)
        {
            try
            {
                StockItem stockItem = StockItemLogic.GetEntityBy(s => s.StockItemId == model.StockItemId);
                Staff staff = _staffLogic.GetEntityBy(p => p.StaffId == model.StaffId);

                if (staff == null)
                {
                    return BadRequest(null, (int)EnumClass.HttpStatus.Error, "Staff not found", false);
                }
                if (stockItem != null)
                {
                    stockItem.Details = model.DetailsAboutStockItem;
                    stockItem.Quantity = model.Quantity;
                    stockItem.StockItemName = model.StockItemName;
                    stockItem.ImageUrl = model.ImageUrl;
                    stockItem.StaffId = model.StaffId;
                    service.Save();
                    return Ok(stockItem, (int)EnumClass.HttpStatus.Success, "Updated successfully", true);
                }
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, "not found", false);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.HttpStatus.Error, ex.Message, false);
            }
        }

        [HttpGet("GetAllStockItems")]
        public IActionResult GetAllStockItems()
        {
            List<StockItem> stockItems = StockItemLogic.GetAllEntities();
            return Ok(stockItems, (int)EnumClass.HttpStatus.Success, "all stockitems", true);
        }


    }
}
