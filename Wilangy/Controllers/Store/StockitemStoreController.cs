﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers.Store
{
    [Route("api/[controller]")]
    public class StockitemStoreController : BaseAPIController
    {
        StockItemStoreLogic StockItemStoreLogic;
        StockItemLogic StockItemLogic;
        StockitemRequestLogic _StockitemRequestLogic;
        IService Service;

        public StockitemStoreController(StockItemStoreLogic stockItemStoreLogic, IService service, StockItemLogic stockItemLogic, StockitemRequestLogic stockitemRequest)
        {
            StockItemStoreLogic = stockItemStoreLogic;
            StockItemLogic = stockItemLogic;
            _StockitemRequestLogic = stockitemRequest;
            Service = service;
        }

        [HttpPost("AddStockItemToStore")]
        public IActionResult AddStockItemToStore(StockitemStoreDTO model)
        {
            try
            {
                StockItem stockitem = StockItemLogic.GetEntityBy(s => s.StockItemId == model.StockItemId);
                int remainingQuantity = 0;
                if (model.Status == (int)EnumClass.StockitemStatus.Removed)
                {
                    remainingQuantity = stockitem.Quantity - model.Quantity;
                }
                if (remainingQuantity >= 0)
                {
                    StockItemStore stockItemStore = new StockItemStore
                    {
                        DateOut = model.DateOut,
                        MoreDetails = model.MoreDetails,
                        NoAvailable = stockitem.Quantity,
                        NoReturned = model.NoReturned,
                        Quantity = model.Quantity,
                        PersonWithStockItem = model.PersonWithStockItem,
                        ReturnDate = model.ReturnDate,
                        StaffId = model.StaffId,
                        StockItemId = model.StockItemId,
                        WhereStockItemIsUsed = model.WhereStockItemIsUsed,
                        StockitemName = model.StockitemName,
                        DateActiviyWasMade = DateTime.Now,
                        Status = model.Status
                    };

                    if (model.Status == (int)EnumClass.StockitemStatus.Added)
                    {
                        stockitem.Quantity += model.Quantity;
                    }else if(model.Status == (int)EnumClass.StockitemStatus.Removed)
                    {
                        stockitem.Quantity -= model.Quantity;
                    }

                    StockItemStoreLogic.AddEntity(stockItemStore);
                    int count = Service.Save();

                    return Ok(stockItemStore, (int)EnumClass.Status.successful, "stock item added to store", true);

                }
                else
                {
                    return BadRequest(null, (int)EnumClass.Status.unsuccessful, "quantity must be >= 0", false);
                }
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpPost("UpdateStockItemToStore")]
        public IActionResult UpdateStockItemToStore(StockitemStoreDTO model)
        {
            try
            {
                StockItem stockitem = StockItemLogic.GetEntityBy(s => s.StockItemId == model.StockItemId);
                StockItemStore stockItemStore = new StockItemStore
                {
                    DateOut = model.DateOut,
                    MoreDetails = model.MoreDetails,
                    NoAvailable = stockitem.Quantity,
                    NoReturned = model.NoReturned,
                    Quantity = model.Quantity,
                    PersonWithStockItem = model.PersonWithStockItem,
                    ReturnDate = model.ReturnDate,
                    StaffId = model.StaffId,
                    StockItemId = model.StockItemId,
                    WhereStockItemIsUsed = model.WhereStockItemIsUsed
                };
                StockItemStoreLogic.AddEntity(stockItemStore);
                int count = Service.Save();

                var data = Mapper.Map<StockitemStoreDTO>(stockItemStore);
                return Ok(data, (int)EnumClass.Status.successful, "stock item added to store", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpGet("ActivitiesForAStockitem")]
        public IActionResult ActivitiesForAStockitem(string stockitemName)
        {
            try
            {
                List<StockItemStore> stockItemStore = StockItemStoreLogic.GetEntitiesBy(s => s.StockitemName == stockitemName);

                return Ok(stockItemStore, (int)EnumClass.Status.successful, "stockitem activities", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpGet("GetAllUnreturnedItems")]
        public IActionResult GetAllUnreturnedItems()
        {
            List<StockItemStore> itemStores = StockItemStoreLogic.GetEntitiesBy(s => s.Quantity > 0);
            return Ok(itemStores, (int)EnumClass.HttpStatus.Success, "unreturned stockitems", true);
        }


        [HttpPost("RequestForStockitem")]
        public IActionResult RequestStockitem(StockitemRequestDTO model)
        {
            try
            {
                StockitemRequest stockitemRequest = new StockitemRequest
                {
                    EstimatedPrice = model.EstimatedPrice,
                    NumberOfItems = model.NumberOfItems,
                    RequestName = model.RequestName,
                    StaffMakingRequest = model.StaffMakingRequest,
                    StoreToPurchaseFrom = model.StoreToPurchaseFrom,
                    VehicleName = model.VehicleName,
                    Status = model.Status
                };
                _StockitemRequestLogic.AddEntity(stockitemRequest);
                Service.Save();
                return Ok(model, (int)EnumClass.HttpStatus.Success, "Request sent", true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }


        [HttpPut("EditStockitemRequest")]
        public IActionResult EditRequest(StockitemRequestDTO model)
        {
            try
            {
                StockitemRequest stockitem = _StockitemRequestLogic.GetEntityBy(p => p.StockitemRequestId == model.StockitemRequestId);
                if(stockitem != null)
                {
                    stockitem.EstimatedPrice = model.EstimatedPrice;
                    stockitem.NumberOfItems = model.NumberOfItems;
                    stockitem.RequestName = model.RequestName;
                    stockitem.StaffMakingRequest = model.StaffMakingRequest;
                    stockitem.Status = model.Status;
                    stockitem.StoreToPurchaseFrom = model.StoreToPurchaseFrom;
                    stockitem.VehicleName = model.VehicleName;
                    
                    Service.Save();
                    return Ok(model, (int)EnumClass.HttpStatus.Success, "changed request", true);
                }
                return BadRequest("Could find this request");
            }catch(Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("GetAllStockitemRequests")]
        public IActionResult GetAllRequests()
        {
            List<StockitemRequest> stockitemRequests = _StockitemRequestLogic.GetAllEntities();
            return Ok(stockitemRequests, (int)EnumClass.HttpStatus.Success, "all requests", true);
        }
    }
}
