﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wilangy.Models.DTO;
using Wilangy.Services.Interfaces;

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : BaseAPIController
    {
        IUploadService _uploadService { get; }

        public UploadController(IUploadService uploadService)
        {
            _uploadService = uploadService;
        }

        [HttpGet("[action]")]
        public IActionResult GetAllUploads(Guid id)
        {
            return Ok(_uploadService.GetUpload(id), (int)EnumClass.HttpStatus.Success, "Success", true);
        }

    }
}
