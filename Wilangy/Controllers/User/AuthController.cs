﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Wilangy.Api.models;
using Wilangy.BusinessLogic;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;
using static Wilangy.Models.DTO.EnumClass;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers.User
{
    [Route("api/[controller]")]
    public class AuthController : BaseAPIController
    {
        IMapper mapper;
        LoginLogLogic loginLogLogic;
        PersonLogic _personLogic;
        public AuthController(LoginLogLogic _loginLogLogic, IMapper _mapper, PersonLogic personLogic)
        {
            loginLogLogic = _loginLogLogic;
            _personLogic = personLogic;
            mapper = _mapper;
        }

        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginDetails viewModel)
        {
            try
            {
                LoginData loginLog = await loginLogLogic.LoginStaff(viewModel);
                //Person person = _personLogic.GetEntityBy(p => p.Email == viewModel.EmailAddress);

                //CompleteLoginData completeLoginData = new CompleteLoginData
                //{
                //    LoginData = loginLog,
                //    Person = person
                //};
                if(loginLog != null)
                {
                    return Ok(loginLog, (int)HttpStatus.Success, "Logged in successfully", true);
                }
                return BadRequest(null, (int)HttpStatus.Error, "Log in failed", false);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
