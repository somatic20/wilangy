﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Wilangy.Api.models;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers.User
{
    [Route("api/[controller]")]
    public class UserController : BaseAPIController
    {
        IMapper mapper;
        UserManager<Person> userManager;
        PersonLogic personLogic;
        PersonTypeLogic personTypeLogic;
        StaffLogic staffLogic;
        IService service;
        RoleManager<IdentityRole> roleManager;

        public UserController(UserManager<Person> _userManager, PersonLogic _personLogic, IMapper _mapper, IService _service, StaffLogic _staffLogic, RoleManager<IdentityRole> _roleManager, PersonTypeLogic _personTypeLogic)
        {
            userManager = _userManager;
            personLogic = _personLogic;
            mapper = _mapper;
            service = _service;
            staffLogic = _staffLogic;
            roleManager = _roleManager;
            personTypeLogic = _personTypeLogic;
        }
        /// <summary>
        /// create a user with staff details and roles
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("CreateUser")]
        public async Task<IActionResult> CreateUser(UserDTO model)
        {
            try
            {
                if (model != null)
                {
                    Person person = personLogic.CreateIdentityUser(model.PersonDTO);
                    if (person != null)
                    {
                        Staff staff = staffLogic.CreateStaff(model.StaffDTO, person.Id);
                        staffLogic.AddEntity(staff);
                        IdentityResult identityResult = await userManager.CreateAsync(person, model.PersonDTO.PhoneNumber);
                        if (model.PersonDTO.PersonTypeId == (int)EnumClass.StaffType.OfficeStaff)
                        {
                            await userManager.AddToRoleAsync(person, model.PersonDTO.Role);
                        }


                        //await AssignClaimsToIdentityUser(model.PersonDTO.Claims, person);

                        int count = service.Save();
                        UserViewModel viewModel = personLogic.CompleteUserDetails(person, staff);
                        return Ok(viewModel, (int)EnumClass.Status.successful, "UserRegistrationSucceded", true);

                    }
                    return BadRequest(null, (int)EnumClass.Status.unsuccessful, "AlreadyRegistered", false);

                }
                return BadRequest(null, (int)EnumClass.Status.successful, "ModelStateInvalid", false);

            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }
        /// <summary>
        /// update a users detaiils
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("UpdateUser")]
        public async Task<IActionResult> UpdateUser(UserDTO model)
        {
            try
            {
                Person person = personLogic.UpdatePerson(model.PersonDTO);
                if (person != null)
                {
                    Staff staff = staffLogic.UpdateStaffDetails(model.StaffDTO, person.Id);

                    if (model.PersonDTO.Role != null)
                    {
                        var role = await userManager.GetRolesAsync(person);

                        await userManager.RemoveFromRoleAsync(person, role[0]);
                        await userManager.AddToRoleAsync(person, model.PersonDTO.Role);
                    }

                    int count = service.Save();
                    await userManager.UpdateAsync(person);
                    UserViewModel viewModel = personLogic.CompleteUserDetails(person, staff);
                    return Ok(viewModel, (int)EnumClass.Status.successful, "updated", true);

                }
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, "User not found", false);

            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        /// <summary>
        /// create various person types
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost("addpersontype")]
        public IActionResult Addpersontype(string name)
        {
            try
            {
                PersonType person = new PersonType
                {
                    Name = name
                };
                personTypeLogic.AddEntity(person);
                service.Save();
                return Ok(person);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, "Couldn't add type", false);
            }
        }
        /// <summary>
        /// Show various enums assigned to users
        /// </summary>
        /// <returns></returns>
        [HttpGet("GetPersonTypes")]
        public IActionResult GetPersonTypes()
        {
            List<PersonType> personTypes = personTypeLogic.GetAllEntities();
            return Ok(personTypes, (int)EnumClass.Status.successful, "all person types", true);
        }

        /// <summary>
        /// add identity role
        /// </summary>
        /// <param name="role"></param>
        /// <returns></returns>
        [HttpPost("CreateRole")]
        public async Task<IActionResult> CreateRole(string role)
        {
            try
            {
                if (!await roleManager.RoleExistsAsync(role))
                {
                    await roleManager.CreateAsync(new IdentityRole(role));
                    return Ok(role, (int)EnumClass.Status.successful, "added role", true);
                }
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, "role add failed", false);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpPost("CreateRoleClaims")]
        public async Task<IActionResult> CreateRoleClaims(string role, string[] claims)
        {
            try
            {
                var userRole = await roleManager.FindByNameAsync(role);
                for (int i = 0; i < claims.Length; i++)
                {
                    string claim = claims[i];
                    var ClaimCheck = (await roleManager.GetClaimsAsync(userRole)).Where(p => p.ValueType == "Permission" && p.Value == claim);
                    if (ClaimCheck.Count() <= 0)
                    {
                        await roleManager.AddClaimAsync(userRole, new Claim("Permission", claim));
                    }
                }
                var data = roleManager.Roles.ToList();

                return Ok(data, (int)EnumClass.Status.successful, "added claims", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpGet("GetAllRoles")]
        public IActionResult GetRoles()
        {
            try
            {
                var roles = roleManager.Roles.ToList();
                return Ok(roles, (int)EnumClass.Status.successful, "all roles", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }


        [HttpGet("GetClaimsByRoles")]
        public async Task<IActionResult> GetClaimsByRole(string role)
        {
            try
            {
                var userRole = await roleManager.FindByNameAsync(role);
                var claims = (await roleManager.GetClaimsAsync(userRole));
                return Ok(claims, (int)EnumClass.Status.successful, "all claims", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }

        [HttpGet("GetAllDrivers")]
        public IActionResult GetAllDrivers()
        {
            try
            {
                List<Person> persoms = personLogic.GetEntitiesBy(p => p.PersonTypeId == (int)EnumClass.StaffType.Driver);

                return Ok(persoms, (int)EnumClass.Status.successful, "all drivers", true);
            }
            catch (Exception ex)
            {
                return BadRequest(null, (int)EnumClass.Status.unsuccessful, ex.Message, false);
            }
        }


        [HttpGet("GetAllStaff")]
        public IActionResult GetAllStaff()
        {
            ICollection<AllStaffView> staff = service.GetAllView<AllStaffView>();

            return Ok(staff, (int)EnumClass.HttpStatus.Success, "allStaff", true);
        }



        [HttpPost("[action]")]
        public async Task<IActionResult> ChangePassword(string email, string oldPhone, string phoneNo)
        {
            Person person = personLogic.GetEntityBy(p => p.Email == email);
            IdentityResult identityResult = await userManager.ChangePasswordAsync(person, oldPhone, phoneNo);
            if (identityResult.Succeeded)
            {
                person.PhoneNumber = phoneNo;
                return Ok("Done");
            }
            return BadRequest("Failed");
        }
        //[HttpGet("GetAllUserRoles")]
        //public async Task<IActionResult> GetUserRole(string userId)
        //{
        //    try
        //    {
        //        Person person = personLogic.GetEntityBy(p => p.Id == userId);
        //        if (person != null)
        //        {
        //            IList<string> role = await userManager.GetRolesAsync(person);
        //            if (role != null)
        //            {
        //                return Ok(role, (int)Enums.Status.successful, "all roles", true);
        //            }
        //        }
        //        return BadRequest(null, (int)Enums.Status.unsuccessful, "not found", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(null, (int)Enums.Status.unsuccessful, ex.Message, false);
        //    }
        //}


        //public async Task<IActionResult> GetUserClaims(string userId)
        //{
        //    try
        //    {
        //        Person person = personLogic.GetEntityBy(c => c.Id == userId);
        //        if (person != null)
        //        {
        //            IList<Claim> claims = await userManager.GetClaimsAsync(person);
        //            return Ok(claims, (int)Enums.Status.successful, "all roles", true);
        //        }
        //        return BadRequest(null, (int)Enums.Status.unsuccessful, "not found", false);
        //    }
        //    catch (Exception ex)
        //    {
        //        return BadRequest(null, (int)Enums.Status.unsuccessful, ex.Message, false);
        //    }
        //}


        /// <summary>
        /// Assign claims to user
        /// </summary>
        /// <param name="Claims"></param>
        /// <param name="person"></param>
        /// <returns></returns>
        /// 
        [NonAction]
        private async Task AssignClaimsToIdentityUser(string[] Claims, Person person)
        {
            for (int i = 0; i < Claims.Length; i++)
            {
                string personType = Enum.GetValues(typeof(EnumClass.StaffType)).Cast<EnumClass.StaffType>().Where(p => (int)p == person.PersonTypeId).Select(v => v.ToString()).FirstOrDefault();
                string claim = Claims[i];
                var ClaimCheck = (await userManager.GetClaimsAsync(person)).Where(p => p.ValueType == personType && p.Value == claim);
                if (ClaimCheck.Count() <= 0)
                {
                    await userManager.AddClaimAsync(person, new Claim(personType, claim));
                }
            }
        }


    }
}
