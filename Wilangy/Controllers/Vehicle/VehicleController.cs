﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    public class VehicleController : BaseAPIController
    {
        VehicleLogic VehicleLogic;
        IService Service;
        PersonLogic PersonLogic;

        public VehicleController(VehicleLogic vehicleLogic, IService service, PersonLogic personLogic)
        {
            VehicleLogic = vehicleLogic;
            Service = service;
            PersonLogic = personLogic;
        }

        [HttpPost("AddVehicle")]
        public IActionResult AddVehicle(VehicleDTO model)
        {
            Vehicle vehicle = VehicleLogic.GetEntityBy(v => string.Equals(v.Name, model.Name, StringComparison.InvariantCultureIgnoreCase));
            if (vehicle == null)
            {
                Person person = PersonLogic.GetEntityBy(p => p.Id == model.DriverId);

                vehicle = new Vehicle
                {
                    Name = model.Name,
                    Details = model.Details,
                    EnteredBy = model.EnteredBy,
                    DriverId = model.DriverId,
                    ImageUrl = model.ImageUrl,
                    DateIn = model.DateIn == null ? DateTime.Now.Date : model.DateIn,
                    DriversName  = $"{person.Lastname} {person.Firstname}",
                    PlateNumber = model.PlateNumber
                };
                VehicleLogic.AddEntity(vehicle);
                int count = Service.Save();
                return Ok(vehicle, (int)EnumClass.HttpStatus.Success, "success", true);
            }
            return BadRequest(null, (int)EnumClass.HttpStatus.Error, "Already Exists", false);
        }

        [HttpPut("UpdateVehicleDetails")]
        public IActionResult UpdateVehicle(VehicleDTO model)
        {
            Vehicle vehicle = VehicleLogic.GetEntityBy(v => v.VehicleId == model.VehicleId);
            if (vehicle != null)
            {
                Person person = PersonLogic.GetEntityBy(p => p.Id == model.DriverId);
                vehicle.Name = model.Name;
                vehicle.Details = model.Details;
                vehicle.EnteredBy = model.EnteredBy;
                vehicle.ImageUrl = model.ImageUrl;
                vehicle.DriversName = $"{person.Lastname} {person.Firstname}";
                vehicle.PlateNumber = model.PlateNumber;
                int count = Service.Save();
                return Ok(vehicle, (int)EnumClass.HttpStatus.Success, "updated", true);
            }
            return BadRequest(null, (int)EnumClass.HttpStatus.Error, "notFound", false);
        }


        [HttpGet("GetAllVehicles")]
        public IActionResult GetAllVehicles()
        {
            List<Vehicle> vehicles = VehicleLogic.GetAllEntities();
            return Ok(vehicles, (int)EnumClass.HttpStatus.Success, "allVehicles", true);
        }
    }
}
