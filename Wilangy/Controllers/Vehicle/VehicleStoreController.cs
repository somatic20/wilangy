﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Wilangy.Api.Controllers
{
    [Route("api/[controller]")]
    public class VehicleStoreController : BaseAPIController
    {
        VehicleStoreLogic vehicleStoreLogic;
        IService Service;

        public VehicleStoreController(VehicleStoreLogic _vehicleStoreLogic, IService _Service)
        {
            vehicleStoreLogic = _vehicleStoreLogic;
            Service = _Service;
        }

        [HttpPost("AddVehicleActivityToStore")]
        public IActionResult AddVehicleToStore([FromBody]VehicleStoreDTO vehicleStoreDTO)
        {
            try
            {
                int count = 0;
                for (int i = 0; i < vehicleStoreDTO.StockitemList.Count; i++)
                {
                    VehicleStore vehicleStore = new VehicleStore
                    {
                        DateEntered = vehicleStoreDTO.DateEntered == null ? DateTime.Now : vehicleStoreDTO.DateEntered,
                        Location = vehicleStoreDTO.Location,
                        RepairDetails = vehicleStoreDTO.Details,
                        StaffId = vehicleStoreDTO.StaffId,
                        TotalCostOfRepairs = vehicleStoreDTO.TotalCostOfRepairs,
                        VehicleId = vehicleStoreDTO.VehicleId,
                        StaffWithVehicle = vehicleStoreDTO.StaffWithVehicle,
                        VehicleName = vehicleStoreDTO.VehicleName,
                        StockItemName = vehicleStoreDTO.StockitemList[i].StockItemName,
                        Quantity = vehicleStoreDTO.StockitemList[i].Quantity
                    };

                    vehicleStoreLogic.AddEntity(vehicleStore);
                    count = Service.Save();
                }

                if (count > 0)
                {
                    return Ok(vehicleStoreDTO, (int)EnumClass.HttpStatus.Success, "Added to store", true);

                }
                else
                {
                    return BadRequest(vehicleStoreDTO, (int)EnumClass.HttpStatus.Error, "Couldn't add activity", false);

                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpPut("UpdateVehicleActivityToStore")]
        public IActionResult UpdateVehicleToStore(VehicleStoreDTO vehicleStoreDTO)
        {
            try
            {
                VehicleStore VehicleStore = vehicleStoreLogic.GetEntityBy(v => v.VehicleName == vehicleStoreDTO.VehicleName);

                if (VehicleStore != null)
                {
                    VehicleStore.Location = vehicleStoreDTO.Location;
                    VehicleStore.RepairDetails = vehicleStoreDTO.Details;
                    VehicleStore.TotalCostOfRepairs = vehicleStoreDTO.TotalCostOfRepairs;
                    VehicleStore.VehicleId = vehicleStoreDTO.VehicleId;
                    //VehicleStore.StockItemName = vehicleStoreDTO.StockItemId;
                    VehicleStore.StaffWithVehicle = vehicleStoreDTO.StaffWithVehicle;

                    int count = Service.Save();
                    return Ok(VehicleStore, (int)EnumClass.HttpStatus.Success, "Added to store", true);
                }
                else
                {
                    return BadRequest(VehicleStore, (int)EnumClass.HttpStatus.Error, "Vehicle not found", false);
                }

            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [HttpGet("ActivitiesForAVehicle")]
        public IActionResult ActivitiesForAVehicle(string vehicleName)
        {
            try
            {
                List<VehicleStore> vehicleStores = vehicleStoreLogic.GetEntitiesBy(v => v.VehicleName == vehicleName);
                return Ok(vehicleStores, (int)EnumClass.HttpStatus.Success, "all activities", true);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }

        }
    }
}
