﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wilangy.Api.Migrations
{
    public partial class _4 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_StaffTasks_Staff_StaffId",
                table: "StaffTasks");

            migrationBuilder.DropIndex(
                name: "IX_StaffTasks_StaffId",
                table: "StaffTasks");

            migrationBuilder.AlterColumn<string>(
                name: "StaffId",
                table: "StaffTasks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "StaffId",
                table: "StaffTasks",
                nullable: true,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_StaffTasks_StaffId",
                table: "StaffTasks",
                column: "StaffId");

            migrationBuilder.AddForeignKey(
                name: "FK_StaffTasks_Staff_StaffId",
                table: "StaffTasks",
                column: "StaffId",
                principalTable: "Staff",
                principalColumn: "StaffId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
