﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Wilangy.Api.Migrations
{
    public partial class _5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "FIle",
                table: "Uploads",
                newName: "UrlLink");

            migrationBuilder.AddColumn<int>(
                name: "ItemId",
                table: "Uploads",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "ContractId",
                table: "Reports",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateEntered",
                table: "Reports",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "EnteredBy",
                table: "Reports",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "DateEntered",
                table: "Correspondences",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "EnteredBy",
                table: "Correspondences",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "MaintenanceLogs",
                columns: table => new
                {
                    MaintenanceLogId = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    VehicleId = table.Column<int>(nullable: false),
                    Details = table.Column<string>(nullable: true),
                    EnteredBy = table.Column<string>(nullable: true),
                    DateEntered = table.Column<DateTime>(nullable: false),
                    VehicleId0 = table.Column<int>(name: "[VehicleId]", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MaintenanceLogs", x => x.MaintenanceLogId);
                    table.ForeignKey(
                        name: "FK_MaintenanceLogs_Vehicle_[VehicleId]",
                        column: x => x.VehicleId0,
                        principalTable: "Vehicle",
                        principalColumn: "VehicleId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceLogs_[VehicleId]",
                table: "MaintenanceLogs",
                column: "[VehicleId]");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MaintenanceLogs");

            migrationBuilder.DropColumn(
                name: "ItemId",
                table: "Uploads");

            migrationBuilder.DropColumn(
                name: "ContractId",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "DateEntered",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "EnteredBy",
                table: "Reports");

            migrationBuilder.DropColumn(
                name: "DateEntered",
                table: "Correspondences");

            migrationBuilder.DropColumn(
                name: "EnteredBy",
                table: "Correspondences");

            migrationBuilder.RenameColumn(
                name: "UrlLink",
                table: "Uploads",
                newName: "FIle");
        }
    }
}
