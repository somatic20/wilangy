﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Wilangy.Api.Migrations
{
    public partial class _8 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaintenanceLogs_Vehicle_[VehicleId]",
                table: "MaintenanceLogs");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceLogs_[VehicleId]",
                table: "MaintenanceLogs");

            migrationBuilder.DropColumn(
                name: "[VehicleId]",
                table: "MaintenanceLogs");

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceLogs_VehicleId",
                table: "MaintenanceLogs",
                column: "VehicleId");

            migrationBuilder.AddForeignKey(
                name: "FK_MaintenanceLogs_Vehicle_VehicleId",
                table: "MaintenanceLogs",
                column: "VehicleId",
                principalTable: "Vehicle",
                principalColumn: "VehicleId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_MaintenanceLogs_Vehicle_VehicleId",
                table: "MaintenanceLogs");

            migrationBuilder.DropIndex(
                name: "IX_MaintenanceLogs_VehicleId",
                table: "MaintenanceLogs");

            migrationBuilder.AddColumn<int>(
                name: "[VehicleId]",
                table: "MaintenanceLogs",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_MaintenanceLogs_[VehicleId]",
                table: "MaintenanceLogs",
                column: "[VehicleId]");

            migrationBuilder.AddForeignKey(
                name: "FK_MaintenanceLogs_Vehicle_[VehicleId]",
                table: "MaintenanceLogs",
                column: "[VehicleId]",
                principalTable: "Vehicle",
                principalColumn: "VehicleId",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
