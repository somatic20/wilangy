﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wilangy.Models.Entity;

namespace Wilangy.Api
{
    public static class SeederInfrastructure
    {
        public static void Initialize(ApplicationDbContext context)
        {
            if (!context.Gender.Any())
            {
                List<Gender> genders = new List<Gender>
                {
                    new Gender
                    {
                        Name = "Male"
                    },
                    new Gender
                    {
                        Name = "Female"
                    }
                };
                context.AddRange(genders);
            }

        }
    }
}
