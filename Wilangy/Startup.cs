﻿using System;
using System.Text;
using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using Object.Jwt;
using Swashbuckle.AspNetCore.Swagger;
using Wilangy.Api;
using Wilangy.BusinessLogic;
using Wilangy.Data;
using Wilangy.Models.Entity;
using Wilangy.Services;
using Wilangy.Services.Interfaces;

namespace Wilangy
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(
                options =>
                {
                    options.EnableSensitiveDataLogging();
                    options
                .UseLazyLoadingProxies(useLazyLoadingProxies:false)
                .UseSqlServer(Configuration.GetConnectionString("DefaultConnection"), b => b.MigrationsAssembly("Wilangy.Api"));
                    options.ConfigureWarnings(c => c.Log(CoreEventId.DetachedLazyLoadingWarning));
                }
            );

            services.AddCors();

            services.ConfigureApplicationCookie(options =>
            {
                // Cookie settings
                options.Cookie.HttpOnly = true;
                options.ExpireTimeSpan = TimeSpan.FromMinutes(30);
                // If the LoginPath isn't set, ASP.NET Core defaults 
                // the path to /Account/Login.
                options.LoginPath = "/Account/Login";
                // If the AccessDeniedPath isn't set, ASP.NET Core defaults 
                // the path to /Account/AccessDenied.
                options.AccessDeniedPath = "/Account/AccessDenied";
                options.SlidingExpiration = true;
            });

            var builder = services.AddIdentity<Person, IdentityRole>(o =>
            {
                // configure identity options
                o.Password.RequireDigit = false;
                o.Password.RequireLowercase = false;
                o.Password.RequireUppercase = false;
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 6;
            });

            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder.AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();

            // JWT Configuration======
            var jwtAppSettingOptions = Configuration.GetSection(nameof(JwtIssuerOptions));


            // Configure JwtIssuerOptions
            SymmetricSecurityKey _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration["JwtIssuerOptions:SecretKey"]));
            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)];
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtAppSettingOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;

            }).AddJwtBearer(configureOptions =>
            {
                configureOptions.ClaimsIssuer = jwtAppSettingOptions[nameof(JwtIssuerOptions.Issuer)];
                configureOptions.TokenValidationParameters = tokenValidationParameters;
                configureOptions.SaveToken = true;
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddScoped<IJwtFactory, JwtFactory>();
            services.AddScoped<IService, Service>();
            services.AddTransient<ContractLogic>();
            services.AddTransient<GenderLogic>();
            services.AddTransient<PersonLogic>();
            services.AddTransient<PersonTypeLogic>();
            services.AddTransient<StaffLogic>();
            services.AddTransient<StockItemLogic>();
            services.AddTransient<StockItemStoreLogic>();
            services.AddTransient<VehicleLogic>();
            services.AddTransient<VehicleStoreLogic>();
            services.AddTransient<LoginLogLogic>();
            services.AddTransient<JwtIssuerOptions>();
            services.AddTransient<ContractLogic>();
            services.AddTransient<AccountLogic>();
            services.AddTransient<SubContractorLogic>();
            services.AddTransient<SubContractorBidLogic>();
            services.AddTransient<StockitemRequestLogic>();
            services.AddTransient<UploadLogic>();
            services.AddTransient<ReportLogic>();
            services.AddTransient<CorrespondenceLogic>();
            services.AddTransient<StaffTaskLogic>();
            services.AddTransient<MaintenanceLogLogic>();
            services.AddAutoMapper();

            services.AddTransient<IStaffTaskService, StaffTaskService>();
            services.AddTransient<IMaintenanceLogService, MaintenanceLogService>();
            services.AddTransient<ICorrespondenceService, CorrespondenceService>();
            services.AddTransient<IReportService, ReportService>();
            services.AddTransient<IUploadService, UploadService>();



            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info
                {
                    Version = "v1",
                    Title = "Wilangy API",
                    Description = "Wilangy ASP.NET Core Web API",
                });
            });

            //var applicationDbContext = services
            //      .BuildServiceProvider()
            //      .GetRequiredService<ApplicationDbContext>();

            //applicationDbContext.Database.Migrate();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env,ApplicationDbContext context)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();
            app.UseCors(build => build.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader());
            app.UseStaticFiles();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Wilangy API V1");
            });

            //add auth dependency
            app.UseAuthentication();

            app.UseHttpsRedirection();
            SeederInfrastructure.Initialize(context);
            app.UseMvc();
        }
    }
}
