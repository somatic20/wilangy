﻿using AutoMapper;
using System.Collections.Generic;
using Wilangy.Models.DTO;
using Wilangy.Models.Entity;

namespace Wilangy.Api.models
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<PersonDTO, Person>();
            CreateMap<Person, PersonDTO>();
            CreateMap<StockitemDTO, StockItem>();
            CreateMap<StockItem, StockitemDTO>();
            CreateMap<Contract, ContractDTO>();
            CreateMap<ContractDTO, Contract>();
        }
    }
}
