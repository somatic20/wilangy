﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wilangy.Api.models
{
    public class PersonViewModel
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Othername { get; set; }
        public string Email { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public int GenderId { get; set; }
        public string ContactAddress { get; set; }
        public string HomeAddress { get; set; }
        public string Lga { get; set; }
        public string WorkAddress { get; set; }
        public string CompanyName { get; set; }
        public string PictureUrl { get; set; }
        public string EnteredBy { get; set; }
        public int PersonTypeId { get; set; }
    }
}
