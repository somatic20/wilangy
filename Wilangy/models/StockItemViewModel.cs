﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Wilangy.Api.models
{
    public class StockItemViewModel
    {
        public int StockItemId { get; set; }
        [Required]
        public string StockItemName { get; set; }
        [Required]
        public int Quantity { get; set; }
        [Required]
        public string StaffId { get; set; }
        public string DetailsAboutStockItem { get; set; }

    }
}
